/* This is a simplified version of
https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/-/blob/master/internal/logging/logger.go.

Special thanks to the GitLab Runner team for making it publicly available!*/

package logging

import "io"

type Logger interface {
	Info(args ...interface{})
	Infof(format string, args ...interface{})
	Warning(args ...interface{})
	Warningf(format string, args ...interface{})

	WithField(key string, value interface{}) Logger
	WithFields(fields Fields) Logger

	SetOutput(w io.Writer)
}

func New() Logger {
	return newLogrus()
}
