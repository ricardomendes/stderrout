package stage

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/ricardomendes/stderrout"
	"gitlab.com/ricardomendes/stderrout/logging"
)

type CustomExecutorConfig struct {
	Driver DriverConfig `json:"driver"`
}

type DriverConfig struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

func NewConfigStage(ctx context.Context, logger logging.Logger) Config {
	configStage := &Config{
		ctx:    ctx,
		logger: logger.WithField("command", "config_exec"),
	}

	return *configStage
}

type Config struct {
	ctx    context.Context
	logger logging.Logger
}

func (c Config) Exec() error {
	c.logger.Info("The CONFIG stage is processed here")

	return c.writeCustomExecutorConfig()
}

func (c Config) writeCustomExecutorConfig() error {
	driverConfig := &DriverConfig{
		Name:    stderrout.Name,
		Version: stderrout.Version().ShortLine(),
	}

	customExecutorConfig := &CustomExecutorConfig{
		Driver: *driverConfig,
	}

	config, err := json.Marshal(customExecutorConfig)
	if err != nil {
		return err
	}

	fmt.Println(string(config))

	return nil
}
