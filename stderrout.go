package stderrout

import "fmt"

const (
	Name     = "std err/out"
	Usage    = "std err/out driver for GitLab Runner's Custom Executor"
	version  = "0.1.0"
	revision = "HEAD"

	AuthorName  = "Ricardo Mendes"
	AuthorEmail = "ricardolsmendes@gmail.com"
)

type VersionInfo struct {
	Name     string
	version  string
	revision string
}

func (v *VersionInfo) ShortLine() string {
	return fmt.Sprintf("%s (%s)", v.version, v.revision)
}

var versionInfo *VersionInfo

func Version() *VersionInfo {
	if versionInfo != nil {
		return versionInfo
	}

	versionInfo = &VersionInfo{
		Name:     Name,
		version:  version,
		revision: revision,
	}

	return versionInfo
}
